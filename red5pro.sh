#/usr/bin/bash
apt-get update
apt-get install -y libva1 libva-drm1 libva-x11-1 libvdpau1 default-jre unzip
cd /usr/local && unzip /root/red5pro/red5pro-3.0.2.zip
mv /usr/local/red5pro-server-3.2.0.b154-release/ /usr/local/red5pro
cp /root/red5pro/red5pro /etc/init.d/red5pro
chmod 777 /etc/init.d/red5pro
read -p 'Please insert License key :' key
printf "$key" > /usr/local/red5pro/LICENSE.KEY
echo "================= FINISHED INSTALL RED5PRO ==============="
echo "START SERVER: /etc/init.d/red5pro start "
ip=$(dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | awk -F'"' '{ print $2}')
echo "Go to website :$ip:5080"

